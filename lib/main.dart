import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:sistematizacao/model/currency.dart';
import 'package:sistematizacao/provider/quotes_provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<QuotesProvider>(
            create: (context) => QuotesProvider())
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: MyHomePage(title: 'Trabalho de Sistematização'),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TextEditingController _controller = TextEditingController(text: "100");

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              FutureBuilder<void>(
                future: Provider.of<QuotesProvider>(context, listen: false)
                    .getCurrencies(),
                builder: (context, snapshot) => Column(
                  children: [
                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text("Moeda local: "),
                        ),
                        Consumer<QuotesProvider>(
                          builder: (context, model, child) => Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: DropdownButton<Currency>(
                              value: model.moedaLocal,
                              onChanged: (value) {
                                model.moedaLocal = value;
                                model.getConversion();
                              },
                              items: model.supportedCurrencies.where((element) {
                                if (model.moedaEstrangeira == null) {
                                  return true;
                                } else {
                                  return element.sigla !=
                                      model.moedaEstrangeira!.sigla;
                                }
                              }).map((Currency e) {
                                return DropdownMenuItem<Currency>(
                                    key: Key('moedaLocal_${e.sigla}'),
                                    value: e,
                                    child: Text(e.valor));
                              }).toList(),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Consumer<QuotesProvider>(
                      builder: (context, model, child) => IconButton(
                        icon: Icon(Icons.swap_calls),
                        onPressed: () {
                          model.swapCurrencies();
                        },
                      ),
                    ),
                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text("Moeda estrangeira: "),
                        ),
                        Consumer<QuotesProvider>(
                          builder: (context, model, child) => Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: DropdownButton<Currency>(
                              value: model.moedaEstrangeira,
                              onChanged: (value) {
                                model.moedaEstrangeira = value;
                                model.getConversion();
                              },
                              items: model.supportedCurrencies.where((element) {
                                if (model.moedaLocal == null) {
                                  return true;
                                } else {
                                  return element.sigla !=
                                      model.moedaLocal!.sigla;
                                }
                              }).map((Currency e) {
                                return DropdownMenuItem<Currency>(
                                    key: Key('moedaEstrangeira_${e.sigla}'),
                                    value: e,
                                    child: Text(e.valor));
                              }).toList(),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Consumer<QuotesProvider>(
                        builder: (context, model, child) =>
                            model.singleQuote != null
                                ? Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(
                                        "Uma unidade de ${model.moedaLocal!.valor} vale ${model.singleQuote!.rate} em ${model.moedaEstrangeira!.valor}"),
                                  )
                                : SizedBox())
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text("Converter"),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextFormField(
                  controller: _controller,
                  keyboardType: TextInputType.numberWithOptions(
                      signed: false, decimal: false),
                  decoration: InputDecoration(
                      hintText: "Valor em moeda local", labelText: "Valor"),
                  inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                  onChanged: (value) => Provider.of<QuotesProvider>(context, listen: false).amount = double.parse(value),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child:
                    Consumer<QuotesProvider>(builder: (context, model, child) {
                  if (model.amountQuote != null) {
                    bool unitario =
                        (_controller.text.isEmpty || _controller.text == "1");
                    String text = unitario
                        ? "Uma unidade de ${model.moedaLocal!.valor} vale ${model.amountQuote!.rate} em ${model.moedaEstrangeira!.valor}"
                        : "${_controller.text} unidades de ${model.moedaLocal!.valor} valem ${model.amountQuote!.rate} em ${model.moedaEstrangeira!.valor}";
                    return Text(text);
                  } else {
                    return SizedBox();
                  }
                }),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: ElevatedButton(
                    onPressed: () {
                      Provider.of<QuotesProvider>(context, listen: false)
                          .getConversionwithAmmount();
                    },
                    child: Text("Converter")),
              )
            ],
          ),
        ),
      ),
    );
  }
}
