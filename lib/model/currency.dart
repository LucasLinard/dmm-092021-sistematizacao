class Currency {
  String sigla;
  String valor;

  Currency({required this.sigla, required this.valor});

  Currency.fromJson(Map<String, dynamic> json)
      : sigla = json[''],
        valor = json[''];

  Map<String, dynamic> toJson() => {"sigla": sigla, "valor": valor};
}
