import 'package:sistematizacao/model/currency.dart';

class Quotes {
  num amount;
  String base;
  String date;
  List<Currency> rates;

  Quotes({required this.amount, required this.base, required this.date, required this.rates});
}