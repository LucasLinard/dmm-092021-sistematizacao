class SingleQuote {
  num amount;
  String base;
  String date;
  num rate;

  SingleQuote(
      {required this.amount,
      required this.base,
      required this.date,
      required this.rate});

  SingleQuote.fromJson(Map<String, dynamic> json)
      : amount = json['amount'],
        base = json['base'],
        date = json['date'],
        rate = (json['rates'] as Map<String,dynamic>).values.first;

  Map<String, dynamic> toJson() =>
      {"amount": amount, "base": base, "date": date, "rates": rate};
}
