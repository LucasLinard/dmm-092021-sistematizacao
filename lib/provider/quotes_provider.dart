import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sistematizacao/model/currency.dart';
import 'package:sistematizacao/model/quotes.dart';
import 'dart:convert';

import 'package:sistematizacao/model/single_quote.dart';

class QuotesProvider with ChangeNotifier {
  List<Currency> _supportedCurrencies = [];
  List<Currency> get supportedCurrencies => _supportedCurrencies;
  set supportedCurrencies(List<Currency> supportedCurrencies) {
    _supportedCurrencies = supportedCurrencies;
    notifyListeners();
  }

  Currency? _moedaLocal;
  Currency? get moedaLocal => _moedaLocal;
  set moedaLocal(Currency? moedaLocal) {
    _moedaLocal = moedaLocal;
    SharedPreferences.getInstance().then((value) => value.setString("moedaLocal", moedaLocal!.sigla));
    notifyListeners();
  }

  Currency? _moedaEstrangeira;
  Currency? get moedaEstrangeira => _moedaEstrangeira;
  set moedaEstrangeira(Currency? moedaEstrangeira) {
    _moedaEstrangeira = moedaEstrangeira;
    SharedPreferences.getInstance().then((value) => value.setString("moedaEstrangeira", moedaEstrangeira!.sigla));
    notifyListeners();
  }

  SingleQuote? _singleQuote;
  SingleQuote? get singleQuote => _singleQuote;
  set singleQuote(SingleQuote? singleQuote) {
    _singleQuote = singleQuote;
    notifyListeners();
  }

  SingleQuote? _amountQuote;
  SingleQuote? get amountQuote => _amountQuote;
  set amountQuote(SingleQuote? amountQuote) {
    _amountQuote = amountQuote;
    notifyListeners();
  }

  double _amount = 100;
  double get amount => _amount;
  set amount(double amount) {
    _amount = amount;
    notifyListeners();
  }

  void swapCurrencies(){
    Currency? temp = moedaLocal;
    moedaLocal = moedaEstrangeira;
    moedaEstrangeira = temp;
    getConversion();
    getConversionwithAmmount();
  }

  Future<void> getCurrencies() async {
    var url = Uri.parse('https://www.frankfurter.app/currencies');
    return http.get(url).then((value) {
      Map<String, dynamic> jsonData = json.decode(value.body);
      List<Currency> currencies = [];
      jsonData.keys.forEach((element) {
        currencies.add(Currency(sigla: element, valor: jsonData[element]));
      });
      supportedCurrencies = currencies;
      SharedPreferences.getInstance().then((value) {
        String? siglaLocal = value.getString("moedaLocal");
        String? siglaEstrangeira = value.getString("moedaEstrangeira");

        if(siglaLocal != null) {
          supportedCurrencies.forEach((element) {
            if (element.sigla == siglaLocal) {
              moedaLocal = element;
            }
          });
        }

        if(siglaEstrangeira != null) {
          supportedCurrencies.forEach((element) {
            if (element.sigla == siglaEstrangeira) {
              moedaEstrangeira = element;
            }
          });
        }
      });
    });
  }

  Future<void> getConversion() async {
    if (moedaLocal != null && moedaEstrangeira != null) {
      var url = Uri.parse(
          'https://www.frankfurter.app/latest?from=${moedaLocal!.sigla}&to=${moedaEstrangeira!.sigla}');
      return http.get(url).then((value) {
        Map<String, dynamic> jsonData = json.decode(value.body);
        singleQuote = SingleQuote.fromJson(jsonData);
      });
    }
  }

  Future<void> getConversionwithAmmount() async {
    if (moedaLocal != null && moedaEstrangeira != null) {
      var url = Uri.parse(
          'https://www.frankfurter.app/latest?amount=$amount&from=${moedaLocal!.sigla}&to=${moedaEstrangeira!.sigla}');
      return http.get(url).then((value) {
        Map<String, dynamic> jsonData = json.decode(value.body);
        amountQuote = SingleQuote.fromJson(jsonData);
      });
    }
  }
}
